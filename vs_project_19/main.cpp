//���� ������ � ������� C++ ������ � Visual Studio(���������� ����������).
//
//������� ����� Animal � ��������� ������� Voice() ������� ������� � ������� ������ � �������.
//����������� �� Animal ������� ��� ������(� ������� Dog, Cat � �.�.) � � ��� ����������� ����� Voice() ����� �������, ����� ��� ������� � ������ Dog ����� Voice() ������� � ������� "Woof!".
//� ������� main ������� ������ ���������� ���� Animal � ��������� ���� ������ ��������� ��������� �������.
//����� �������� ������ �� �������, ������� �� ������ �������� ������� ����� Voice().
//�������������� ��� ������, ������ ���������� ��������� �� ����� ������� ����������� Animal.
//
//������� �������� � ���� ������ �� ����������� �� gitlab ����� ����� ����� ��. (���������� �������� ������� ����� ������� visual studio)

#include <vector>
#include <iostream>

enum classAnimal
{
    CAT = 1,
    DOG,
    DUCK
};

class Animal
{
public:
    //int type = 0;

    Animal() {};
    virtual void voice() {};
    virtual void alternativeVoice() {};
};

class Dog : public Animal
{
public:
    Dog() {/* type = DOG; */};
    void voice() override
    {
        std::cout << "Woof";
    }
    void alternativeVoice() override
    {
        std::cout << "Bark";
    }
};

class Cat : public Animal
{
public:
    Cat() {/* type = CAT;*/ };
    void voice() override
    {
        std::cout << "Meow";
    }
    void alternativeVoice() override
    {
        std::cout << "Purr";
    }
};

class Duck : public Animal
{
public:
    Duck(){/* type = DUCK; */};
    void voice() override
    {
        std::cout << "Quack";
    }
    void alternativeVoice() override
    {
        std::cout << "Gul";
    }
};
//
//static void meeting(Animal *a, Animal *b)
//{
//    if (a->type == b->type)
//    {
//        a->alternativeVoice();
//        std::cout << " ";
//        b->alternativeVoice();
//    }
//    else
//    {
//        a->voice();
//        std::cout << " ";
//        b->voice();
//    }
//
//    std::cout << std::endl;
//}

int main() {
    //Animal* a = new Dog();
    //Animal* b = new Cat();
    //Animal* c = new Dog();
    //Animal* d = new Cat();
    //meeting(a, b);
    //meeting(a, c);
    //meeting(b, c);
    //meeting(b, d);

    //Cat barsik(), mursik(), yashka();
    //Dog muhtar(), bobik(), sharik();

    //Animal* a[6] = {&barsik, &muhtar, &mursik, &bobik, &yashka, &sharik };
    
    Animal *a[6];
    a[0] = new Cat();
    a[1] = new Dog();
    a[2] = new Duck();
    a[3] = new Cat();
    a[4] = new Dog();
    a[5] = new Duck();

    //std::vector<Animal*> an;
    //an.push_back(barsik);


    for (int i = 0; i < 6; i++)
    {
        a[i]->voice();
    }

    //delete a;

    return 0;
}

